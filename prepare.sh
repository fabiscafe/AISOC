#! /usr/bin/env bash
############ Environment Setup ############
# Specify the default user of the Archsio. It cant be root, because we're
# running a graphical user session and some apps will just not work
# anymore as root
LIVE_USER='arch'
LIVE_USERS_PASSWD='arch'
# Set your timezone
## https://wiki.archlinux.org/title/Installation_guide#Time_zone
LIVE_TIMEZONE='Europe/Berlin'
# Set your LOCALE
## https://wiki.archlinux.org/title/Installation_guide#Localization
LIVE_LOCALE='de_DE.UTF-8 UTF-8'
# Set your LANG
## https://wiki.archlinux.org/title/Installation_guide#Localization
LIVE_LANG='de_DE.UTF-8'
# Set GNOME xkb-layout
LIVE_GNOME_XKB='de'
# Set your regulatory domain
## https://wiki.archlinux.org/title/Network_configuration/Wireless#Respecting_the_regulatory_domain
LIVE_WIRELESS_REGDOM='DE'
# Set the message of the day, a little text that might be shown on
# the displaymanager
LIVE_MOTD="👋 Willkommen bei Arch Linux ☺️\n👉 Archinstall ist im Menü! 😳\nBuilddate: $(date +%Y-%m-%d)"

# Contact info
YOUR_NAME='Fabian Bornschein'
YOUR_EMAIL='fabiscafe-cat-mailbox-dog-org'

# This is an array of packages who will be included additional
# to the normal archiso packages
ADD_PKGLIST=(
    "chntpw" # NT Password Changer
    "cups"
    "dbus-broker"
    "exfat-utils" # EXFAT Support
    "foomatic-db-gutenprint-ppds"
    "foomatic-db-nonfree-ppds"
    "foomatic-db-ppds"
    "gnome"
    "gnome-initial-setup" # Broken Users Rescue
    "gnome-tour" # Welcome-screen
    "gutenprint"
    "hplip"
    "htop"
    "networkmanager"
    "ntfs-3g" # NTFS-Creation Support
    "pipewire-alsa"
    "pipewire-jack"
    "pipewire"
    "pipewire-pulse"
    "spice-vdagent" # GnomeBoxes/QEMU support
    "system-config-printer"
    "tmate" #easy terminal remote access support
    "ttf-joypixels" # Text-Emoji support
)

# ISO configuration
ISO_NAME='de_vanilla_archiso'
ISO_APPLICATION='Arch Linux Install Disk'

# Profile
# Specify a path where the setup configuration is generated
PROFILE_DIRECTORY="/tmp/archiso"
# Where to build the iso?
# Please specity a path that is not on an encrypted or a compressed device
# Both can lead to a much higher buildtime as well as RAM usage
BUILD_DIRECTORY="/var/cache/archiso"
# Where should the iso file end up?
ISO_DIRECTORY="$HOME"

############## Logic Parts ###############
# Nothing to change here!
## Disallow root usage
if [ $(id -u) -eq 0 ]; then
    printf "Root usage is not allowed\n"
    exit 1
fi

# Exit script on failures
set -e
## Set right permissions for new files/dirs
umask 022
## check for old profile
if [ "${PROFILE_DIRECTORY}" ]; then 
    rm -rf "${PROFILE_DIRECTORY}"
	printf "old profile directory found and removed\n"
fi
## Copy archlive and give user permissions
if [ ! -d /usr/share/archiso/configs/releng ]; then 
	printf "/usr/share/archiso/configs/releng not found. Please check\n"
	printf "https://wiki.archlinux.org/index.php/Archiso\n"
	exit 1
fi
cp -r /usr/share/archiso/configs/releng/ "${PROFILE_DIRECTORY}"

# Iso configuration
sed -i '/^iso_name/d;/^iso_publisher/d;/^iso_application/d' "${PROFILE_DIRECTORY}/profiledef.sh"
printf "iso_name=\"${ISO_NAME}\"\n" >> "${PROFILE_DIRECTORY}/profiledef.sh"
printf "iso_publisher=\"${YOUR_NAME} <${YOUR_EMAIL}>\"\n" >> "${PROFILE_DIRECTORY}/profiledef.sh"
printf "iso_application=\"${ISO_APPLICATION}\"\n" >> "${PROFILE_DIRECTORY}/profiledef.sh"

## Set timezone
ln -sf "/usr/share/zoneinfo/${LIVE_TIMEZONE}" "${PROFILE_DIRECTORY}/airootfs/etc/localtime"

## Set locale
### To avoid archinstall bugging out, this requires a full /etc/locale.gen file.
### This bit downloads the glibc from Arch and extracts it, to ensure the script does have
### the latest one from the repo
curl -L https://archlinux.org/packages/core/x86_64/glibc/download -o /tmp/glib-latest.tar.zstd
tar --extract --file=/tmp/glib-latest.tar.zstd --directory /tmp etc/locale.gen
mv /tmp/etc/locale.gen "${PROFILE_DIRECTORY}/airootfs/etc/locale.gen"
#### Some apps require to have en_US.UTF-8 available. Hardcode this for that reason
sed -i "s|#en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|g" ${PROFILE_DIRECTORY}/airootfs/etc/locale.gen
sed -i "s|#${LIVE_LOCALE}|${LIVE_LOCALE}|g" ${PROFILE_DIRECTORY}/airootfs/etc/locale.gen

mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/pacman.d/hooks"
cat <<LOCALEGEN > "${PROFILE_DIRECTORY}/airootfs/etc/pacman.d/hooks/40-locale-gen.hook"
[Trigger]
Operation = Install
Type = Package
Target = glibc

[Action]
Description = Run locale-gen...
When = PostTransaction
Depends = glibc
Exec = /usr/bin/locale-gen
LOCALEGEN

## Set lang
printf "LANG=${LIVE_LANG}\n" > "${PROFILE_DIRECTORY}/airootfs/etc/locale.conf"

## Set wireless regdom
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/conf.d"
printf "WIRELESS_REGDOM=\"${LIVE_WIRELESS_REGDOM}\"\n" > "${PROFILE_DIRECTORY}/airootfs/etc/conf.d/wireless-regdom"

## Enable Multilib repo
printf "\n" >> "${PROFILE_DIRECTORY}/pacman.conf"
printf "[multilib]\n" >> "${PROFILE_DIRECTORY}/pacman.conf"
printf "Include = /etc/pacman.d/mirrorlist\n" >> "${PROFILE_DIRECTORY}/pacman.conf"

## Systemd enable/mask
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/multi-user.target.wants"
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/sockets.target.wants"
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/systemd/user"

ln -s /dev/null "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/packagekit.service"
ln -s /usr/lib/systemd/system/gdm.service "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/display-manager.service"
ln -s /usr/lib/systemd/system/dbus-broker.service "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/dbus.service"
ln -s /usr/lib/systemd/user/dbus-broker.service "${PROFILE_DIRECTORY}/airootfs/etc/systemd/user/dbus.service"
ln -s /usr/lib/systemd/system/NetworkManager.service "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/multi-user.target.wants/NetworkManager.service"
ln -s /usr/lib/systemd/system/cups.socket "${PROFILE_DIRECTORY}/airootfs/etc/systemd/system/sockets.target.wants/cups.socket"

## Sudo setup
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/sudoers.d"
printf "%%wheel ALL=(ALL) ALL\n" > "${PROFILE_DIRECTORY}/airootfs/etc/sudoers.d/wheel"

## Archinstall Menu Entry!
mkdir -p "${PROFILE_DIRECTORY}/airootfs/usr/share/applications"
cat <<ARCHINSTDESKTOP > "${PROFILE_DIRECTORY}/airootfs/usr/share/applications/org.archlinux.Archinstall.desktop"
[Desktop Entry]
Name=Archinstall
GenericName=Arch Linux Guided Installer
GenericName[de]=Geführter Installer für Arch Linux
Comment=Install Arch Linux
Comment[de]=Arch Linux installieren
Exec=/usr/bin/pkexec /usr/bin/archinstall
StartupNotify=true
Terminal=true
Icon=archlinux-logo
Type=Application
Categories=Utility;
Keywords=AL;archinstall;calamares;archfi;
ARCHINSTDESKTOP

# Set MOTD
printf "${LIVE_MOTD}" > "${PROFILE_DIRECTORY}/airootfs/etc/motd"

## Package enablement
printf "\n" >> "${PROFILE_DIRECTORY}/packages.x86_64"
for (( i=0; i<=$(( ${#ADD_PKGLIST[@]} - 1 )); i++ ));
    do printf "${ADD_PKGLIST[$i]}\n" >> "${PROFILE_DIRECTORY}/packages.x86_64";
done

# User setup
mkdir -p "${PROFILE_DIRECTORY}/airootfs/home/${LIVE_USER}"

# Group setup
printf "root:x:0:root\n" > "${PROFILE_DIRECTORY}/airootfs/etc/group"
printf "adm:x:4:${LIVE_USER}\n" >> "${PROFILE_DIRECTORY}/airootfs/etc/group"
printf "wheel:x:10:${LIVE_USER}\n" >> "${PROFILE_DIRECTORY}/airootfs/etc/group"
printf "uucp:x:14:${LIVE_USER}\n" >> "${PROFILE_DIRECTORY}/airootfs/etc/group"
printf "${LIVE_USER}:x:1000:\n" >> "${PROFILE_DIRECTORY}/airootfs/etc/group"

# Gshadow
printf 'root:!!::root\n' > "${PROFILE_DIRECTORY}/airootfs/etc/gshadow"
printf "${LIVE_USER}:"'!!'"::\n" >> "${PROFILE_DIRECTORY}/airootfs/etc/gshadow"

# Passwd
sed -i "/^${LIVE_USER}/d" "${PROFILE_DIRECTORY}/airootfs/etc/passwd"
printf "${LIVE_USER}:x:1000:1000::/home/${LIVE_USER}:/usr/bin/zsh\n" \
    >> "${PROFILE_DIRECTORY}/airootfs/etc/passwd"

# Shadow
sed -i "/^${_USERNAME}/d" "${PROFILE_DIRECTORY}/airootfs/etc/shadow"
printf "arch:$(openssl passwd -6 ${LIVE_USERS_PASSWD}):14871::::::\n" \
    >> "${PROFILE_DIRECTORY}/airootfs/etc/shadow"

# Autologin
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/gdm"
cat <<GDM > "${PROFILE_DIRECTORY}/airootfs/etc/gdm/custom.conf"
# GDM configuration storage

[daemon]
# Uncomment the line below to force the login screen to use Xorg
#WaylandEnable=false
AutomaticLoginEnable=True
AutomaticLogin=${LIVE_USER}
[security]

[xdmcp]

[chooser]

[debug]
# Uncomment the line below to turn on debugging
#Enable=true
GDM

# GNOME Keybord layout
mkdir -p "${PROFILE_DIRECTORY}/airootfs/etc/xdg/autostart"
cat <<XKBISOURCE > "${PROFILE_DIRECTORY}/airootfs/etc/xdg/autostart/inputsource.desktop"
[Desktop Entry]
Encoding=UTF-8
Exec=dconf write /org/gnome/desktop/input-sources/sources "[('xkb', '${LIVE_GNOME_XKB}')]"
Name=set german keyboard layout
Terminal=false
OnlyShowIn=GNOME
Type=Application
StartupNotify=false
XKBISOURCE

## Remove Old Build Directory
if [ "${BUILD_DIRECTORY}" ]; then
    printf "old build directory found, use sudo to remove\n"
    sudo rm -rf "${BUILD_DIRECTORY}"
	printf "old build directory was removed\n"
fi

printf "Start Build Process\n"
sudo mkarchiso -v -w "${BUILD_DIRECTORY}" -o "${ISO_DIRECTORY}" "${PROFILE_DIRECTORY}"
printf "Clean Up Build Directory\n"
sudo rm -rf "${BUILD_DIRECTORY}"
